# README #

A repository for exploring how synthesizers work. A repository for playing around with synth sounds and music.

* Special emphasis on the Korg Monologue.
* 0.0.1

### Apotheosynth

This is my *stage name*. It is a portmanteau of the word *apotheosis*, which means the highest point or climax, and *synthesizer*, the musical instrument that creates sounds by electronically combining signals and waveforms of different frequencies.